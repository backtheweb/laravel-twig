Twig 2 for Laravel
------------------

Service provider to integrate Twig2. Work "inspired" and "adapted" to work with twig2 from the project [TwigBridge](https://github.com/rcrowe/TwigBridge)
 
## Installation

Not in composer yet :(

## Quick Start

    'providers' => [
        ...
        Backtheweb\Twig\TwigServiceProvider::class,
    ],
    
    'aliases' => [
        ... 
        'Twig' => Backtheweb\Twig\Facade\Twig::class,
    ],
         
    php artisan vendor:publish --provider="Backtheweb\Twig\TwigServiceProvider"

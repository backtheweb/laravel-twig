<?php

return [

    'extension' => 'twig',
    'globals'   => [],

    'environment' => [

        'debug'                 => env('APP_DEBUG', false),
        'charset'               => 'utf-8',
        'base_template_class'   => '\Backtheweb\Twig\Twig\Template',
        'cache'                 => null,
        'auto_reload'           => true,
        'strict_variables'      => false,
        'autoescape'            => 'html',
        'optimizations'         => -1,
    ],

    'extensions' => [

        'Backtheweb\Twig\Extension\Loader\Facades',
        'Backtheweb\Twig\Extension\Loader\Functions',
        'Backtheweb\Twig\Extension\Loader\Filters',

        'Backtheweb\Twig\Extension\Laravel\Dump',
        'Backtheweb\Twig\Extension\Laravel\Auth',
        'Backtheweb\Twig\Extension\Laravel\Url',
        'Backtheweb\Twig\Extension\Laravel\Config',
        'Backtheweb\Twig\Extension\Laravel\Env',
        'Backtheweb\Twig\Extension\Laravel\Gate',
        'Backtheweb\Twig\Extension\Laravel\Request',
        'Backtheweb\Twig\Extension\Laravel\Session',

        'Backtheweb\Twig\Extension\SwitchExtension',
        //'Backtheweb\Twig\Extension\SvgExtension',
        //'Backtheweb\Twig\Extension\FaExtension',

        'Twig_Extensions_Extension_Text',
        'Twig_Extensions_Extension_Intl',
    ],

    'facades' => [
        'Auth',
    ],

    'functions' => [
        'config',
        'app',
        'mix'
    ],
];

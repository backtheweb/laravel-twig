<?php

namespace Backtheweb\Twig\Command;
use Illuminate\Console\Command;

use Twig_Environment;
use Illuminate\Filesystem\Filesystem;

/**
 * Artisan command to clear the Twig cache.
 */
class Clear extends Command
{
    protected $name = 'twig:clear';

    protected $description = 'Clear Twig Cache';

    public function handle()
    {
        $twig     = $this->laravel['twig'];
        $files    = $this->laravel['files'];
        $cacheDir = $twig->getCache();

        $files->deleteDirectory($cacheDir);

        if ($files->exists($cacheDir)) {

            $this->error('Twig cache failed to be cleaned');

        } else {

            $this->info('Twig cache cleaned');
        }
    }

    public function fire()
    {
        return $this->handle();
    }
}
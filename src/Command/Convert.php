<?php

namespace Backtheweb\Twig\Command;
use Backtheweb\Twig\Tool\Importer;
use Illuminate\Console\Command;

use Twig_Environment;
use Illuminate\Filesystem\Filesystem;

/**
 * Artisan command to clear the Twig cache.
 *
 */
class Convert extends Command
{
    protected $name = 'twig:convert';

    protected $description = 'Convert blade to twig templates';

    public function handle()
    {
        $source = [

            base_path() . '/resources/views/layouts/app.blade.php',
            base_path() . '/resources/views/home.blade.php',
            base_path() . '/resources/views/auth/login.blade.php',
            base_path() . '/resources/views/auth/register.blade.php',
            base_path() . '/resources/views/auth/passwords/email.blade.php',
            base_path() . '/resources/views/auth/passwords/reset.blade.php',
        ];

        $bar = $this->output->createProgressBar(count($source));

        foreach ($source as $file) {

            $target = Importer::blade($file)->save();

            $this->line('');
            $this->line($file);
            $this->info($target);
            $bar->advance();
        }

        $bar->finish();
        $this->line('');
    }

    public function fire()
    {
        return $this->handle();
    }
}

<?php

namespace Backtheweb\Twig\Engine;

use Illuminate\View\Compilers\CompilerInterface;
use Exception;
use InvalidArgumentException;
use Backtheweb\Twig\Twig\Template;

/**
 * Compiles Twig templates.
 */
class Compiler implements CompilerInterface
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * Create a new instance of the Twig compiler.
     *
     * @param \Twig_Environment $twig
     */
    public function __construct(\Twig\Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * Returns the instance of Twig used to render the template.
     *
     * @return \Twig_Environment
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * Twig handles this for us. Here to satisfy interface.
     *
     * {@inheritdoc}
     */
    public function getCompiledPath($path)
    {
        return $this->twig->getCacheFilename($path);
    }

    /**
     * Twig handles this for us. Here to satisfy interface.
     *
     * {@inheritdoc}
     */
    public function isExpired($path)
    {
        $time = filemtime($this->getCompiledPath($path));

        return $this->twig->isTemplateFresh($path, $time);
    }

    /**
     * {@inheritdoc}
     */
    public function compile($path)
    {
        try {

            $this->load($path);

        } catch (Exception $ex) {
            // Unable to compile
            // Try running `php artisan twig:lint`
        }
    }

    /**
     * @param $path
     * @return \Twig\TemplateWrapper
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function load($path)
    {
        // Load template

        try {

            //$template = $this->twig->loadTemplate($path);
            $template = $this->twig->load($path);


        } catch (\Twig\Error\LoaderError $e) {

            throw new InvalidArgumentException("Error loading $path: ". $e->getMessage(), $e->getCode(), $e);
        }

        if ($template instanceof Template) {
            // Events are already fired by the View Environment
            $template->setFiredEvents(true);
        }

        return $template;
    }
}

<?php
namespace Backtheweb\Twig\Extension;

use Backtheweb\Twig\Twig\Loader;
use Illuminate\Filesystem\Filesystem;
use Illuminate\View\FileViewFinder;
use Twig\Extension\AbstractExtension;
use Twig\Loader\ChainLoader;
use Twig\TwigFunction;

class FaExtension extends AbstractExtension
{
    public function getName(){
        return 'FaExtension';
    }

    public function getFunctions(){

        return [
            new TwigFunction('fa_brand',    [$this, 'brand'],   ["is_safe" => ["html"]]),
            new TwigFunction('fa_regular',  [$this, 'regular'], ["is_safe" => ["html"]]),
            new TwigFunction('fa_light',    [$this, 'light'],   ["is_safe" => ["html"]]),
            new TwigFunction('fa_solid',    [$this, 'solid'],   ["is_safe" => ["html"]]),
            new TwigFunction('fa',          [$this, 'fa'],      ["is_safe" => ["html"]]),
        ];
    }

    public function fa($name, $class = null){

        return $this->light($name, join( ' ', ['svg-1x', $class]));
    }

    public function brand($name, $class = null, Array $attrs = []) {

        return $this->load('brands' . '/' .  $name, $class, $attrs);
    }

    public function regular($name, $class = null, Array $attrs = []) {

        return $this->load('regular' . '/' . $name, $class, $attrs);
    }

    public function light($name, $class = null, Array $attrs = []) {

        return $this->load('light' . '/' . $name, $class, $attrs);
    }

    public function solid($name, $class = null, Array $attrs = []) {

        return $this->load('solid' . '/' . $name, $class, $attrs);
    }

    public function load($name, $class = null, Array $attrs = [])
    {
        /** @var \Backtheweb\Twig\Twig\Loader $loader */
        $loader = app('twig.loader.fontawesome');

        if( !$loader->exists($name) ){
            return null;
        }

        $file   = $loader->findTemplate($name);
        $source = file_get_contents($file);
        $svg    = simplexml_load_string($source);

        if ($attrs){

            foreach ($attrs as $key => $value) {

                $svg->attributes()->$key = $value;
                //$svg->addAttribute($key, $value);
            }
        }

        if ($class){
            $svg->addAttribute('class', $class);
        }

        // remove annoying xml version added by asXML method
        return str_replace("<?xml version=\"1.0\"?>", '', $svg->asXML());
    }
}

<?php

namespace Backtheweb\Twig\Extension\Laravel;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

use Illuminate\Auth\AuthManager;

/**
 * Access Laravels auth class in your Twig templates.
 */
class Auth extends AbstractExtension
{
    /**
     * @var \Illuminate\Auth\AuthManager
     */
    protected $auth;

    /**
     * Create a new auth extension.
     *
     * @param \Illuminate\Auth\AuthManager
     */
    public function __construct(AuthManager $auth)
    {
        $this->auth = $auth;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'Backtheweb_Twig_Extension_Laravel_Auth';
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {

        return [
            new TwigFunction('auth_check', [$this->auth, 'check']),
            new TwigFunction('auth_guest', [$this->auth, 'guest']),
            new TwigFunction('auth_user',  [$this->auth, 'user']),
            new TwigFunction('auth_guard', [$this->auth, 'guard']),
        ];
    }
}

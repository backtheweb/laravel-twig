<?php

namespace Backtheweb\Twig\Extension\Laravel;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Illuminate\Config\Repository as ConfigRepository;
/**
 * Access Laravels config class in your Twig templates.
 */
class Config extends AbstractExtension
{
    /**
     * @var \Illuminate\Config\Repository
     */
    protected $config;
    /**
     * Create a new config extension
     *
     * @param \Illuminate\Config\Repository
     */
    public function __construct(ConfigRepository $config)
    {
        $this->config = $config;
    }
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'Backtheweb_Extension_Laravel_Config';
    }
    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('config',     [$this->config, 'get']),
            new TwigFunction('config_get', [$this->config, 'get']),
            new TwigFunction('config_has', [$this->config, 'has']),
        ];
    }
}

<?php

namespace Backtheweb\Twig\Extension\Laravel;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Access Laravels url class in your Twig templates.
 */
class Env extends AbstractExtension
{

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'Backtheweb_Twig_Extension_Laravel_Env';
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('env',          [$this,    'env'],         ['is_safe' => ['html']]),
        ];
    }

    public function getFilters()
    {
        return [

            new TwigFilter('env', [$this, 'env'], ['is_safe' => ['html']]),
        ];
    }

    public function env($key, $default = null )
    {
        return env($key, $default);
    }
}

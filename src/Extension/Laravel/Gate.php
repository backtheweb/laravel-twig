<?php

namespace Backtheweb\Twig\Extension\Laravel;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;

/**
 * Access Laravels auth class in your Twig templates.
 */
class Gate extends AbstractExtension
{
    /**
     * @var GateContract
     */
    protected $gate;

    /**
     * Create a new gate extension.
     *
     * @param GateContract $gate
     */
    public function __construct(GateContract $gate)
    {
        $this->gate = $gate;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'Backtheweb_Twig_Extension_Laravel_Gate';
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('can', [$this->gate, 'check']),
        ];
    }
}

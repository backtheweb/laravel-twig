<?php

namespace Backtheweb\Twig\Extension\Laravel;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig_Extension;
use Twig_SimpleFunction;
use Illuminate\Http\Request as HttpRequest;

/**
 * Access Laravels input class in your Twig templates.
 */
class Request extends AbstractExtension
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Create a new input extension
     *
     * @param \Illuminate\Http\Request
     */
    public function __construct(HttpRequest $request)
    {
        $this->request = $request;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'Backtheweb_Twig_Extension_Laravel_Request';
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('input_get', [$this->request, 'input']),
            new TwigFunction('input_old', [$this->request, 'old']),
            new TwigFunction('input_has', [$this->request, 'has']),
            new TwigFunction('old',       [$this->request, 'old']),
        ];
    }
}

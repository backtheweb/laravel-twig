<?php

namespace Backtheweb\Twig\Extension\Laravel;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

use Illuminate\Translation\Translator as LaravelTranslator;

/**
 * Access Laravels translator class in your Twig templates.
 *
 * Class Translator
 * @package Backtheweb\Twig\Extension\Laravel
 */
class Translator extends AbstractExtension
{
    /**
     * @var \Illuminate\Translation\Translator
     */
    protected $translator;

    /**
     * Create a new translator extension
     *
     * @param \Illuminate\Translation\Translator
     */
    public function __construct(LaravelTranslator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'Backtheweb_Twig_Extension_Laravel_Translator';
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('__',           [$this->translator, 'getFromJson'], ['is_safe' => ['html']]),
            new TwigFunction('trans',        [$this->translator, 'trans'],       ['is_safe' => ['html']]),
            new TwigFunction('trans_choice', [$this->translator, 'transChoice'], ['is_safe' => ['html']]),
            new TwigFunction('__x',          [$this->translator, 'getFromJson']),
            new TwigFunction('__n',          [$this,             'plural']),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('__',            [$this->translator, 'getFromJson'], ['pre_escape' => 'html', 'is_safe' => ['html']]),
            new TwigFilter('trans',         [$this->translator, 'trans'],       ['pre_escape' => 'html', 'is_safe' => ['html']]),
            new TwigFilter('trans_choice',  [$this->translator, 'transChoice'], ['pre_escape' => 'html', 'is_safe' => ['html']]),
        ];
    }

    /**
     * @param $key
     * @param $plural
     * @param int $count
     * @param array $replace
     * @param null $locale
     * @return array|string
     */
    public function plural($key, $plural, $count = 1, $replace = [], $locale = null){

        return $count === 1  ?
            $this->translator->getFromJson($key,    $replace,  $locale)  :
            $this->translator->getFromJson($plural, $replace,  $locale)
            ;
    }
}

<?php

namespace Backtheweb\Twig\Extension\Loader;

use Backtheweb\Twig\Extension\Loader\Facade\Caller;
use Backtheweb\Twig\Twig\Globals;

/**
 * Extension to expose defined facades to the Twig templates.
 *
 * See the `extensions.php` config file, specifically the `facades` key
 * to configure those that are loaded.
 *
 * Use the following syntax for using a facade in your application.
 *
 * <code>
 *     {{ Facade.method(param, ...) }}
 *     {{ Config.get('app.timezone') }}
 * </code>
 */
class Facades extends Loader implements Globals
{
    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'Backtheweb_Extension_Loader_Facades';
    }

    /**
     * {@inheritDoc}
     */
    public function getGlobals()
    {
        $load    = $this->config->get('twig.facades', []);
        $globals = [];

        foreach ($load as $facade => $options) {
            list($facade, $callable, $options) = $this->parseCallable($facade, $options);

            $globals[$facade] = new Caller($callable, $options);
        }

        return $globals;
    }
}
<?php
namespace Backtheweb\Twig\Extension;

use Backtheweb\Twig\Twig\Loader;
use Illuminate\View\FileViewFinder;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;



/**
 * @see https://github.com/manuelodelain/svg-twig-extension/blob/master/src/SvgExtension.php
 */
class SvgExtension extends AbstractExtension
{
    public function getName(){
        return 'SvgExtension';
    }

    public function getFunctions(){

        return [
            new TwigFunction('svg', array($this, 'load'), array("is_safe" => array("html")))
        ];
    }

    public function load($name, $class = null, Array $attrs = []) {

        /** @var \Backtheweb\Twig\Twig\Loader $loader */
        $loader = app('twig.loader.svg');

        if(!$loader->exists($name)){

            return null;
        }

        $file   = $loader->findTemplate($name);
        $source = file_get_contents($file);
        $svg    = \simplexml_load_string($source);

        if ($attrs){

            foreach ($attrs as $key => $value) {

                $svg->attributes()->$key = $value;
                //$svg->addAttribute($key, $value);
            }
        }

        if ($class){
            $svg->addAttribute('class', $class);
        }

        // remove annoying xml version added by asXML method
        return str_replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", '', $svg->asXML());
    }
}

<?php
namespace Backtheweb\Twig\Extension;

use Backtheweb\Twig\TokenParser\SwitchTokenParser;
use Twig\Extension\AbstractExtension;

class SwitchExtension extends AbstractExtension
{
    public function getTokenParsers(): array
    {
        return [
            new SwitchTokenParser(),
        ];
    }
    public function getName()
    {
        return 'Backtheweb_Twig_Extension_Switch';
    }
}

<?php
namespace Backtheweb\Twig\Extension;

use Backtheweb\Linguo\Linguo;

use Illuminate\Support\Facades\Lang;
use Illuminate\Translation\Translator as LaravelTranslator;

use Twig\TwigFunction;

/**
 * @deprecated  use \Backtheweb\Twig\Extension\Laravel\Translator
 *
 * Class Translator
 * @package Backtheweb\Twig\Extension
 */
class Translator extends \Twig\Extension\AbstractExtension
{
    /**
     * @var LaravelTranslator
     */
    protected $translator;

    public function __construct(LaravelTranslator $translator)
    {
        $this->translator = $translator;
    }

    public function getName()
    {
        return 'Backtheweb_Linguo_Extension';
    }

    public function getFunctions(){

        return [

            new TwigFunction('__',           [$this, '__'],             ['is_safe' => ['html']]),
            new TwigFunction('trans',        [$this, 'trans'],          ['is_safe' => ['html']]),
            new TwigFunction('trans_choice', [$this, 'transChoice',     ['is_safe' => ['html']]]),
        ];
    }

    public function __($key, array $replace = [], $locale = null)
    {
        return $this->translator->getFromJson($key, $replace, $locale);
    }

    public function trans($key, array $replace = [], $locale = null, $fallback = null)
    {
        return $this->translator->trans($key, $replace, $locale);
    }

    public function transChoice($key, $count, array $replace = [], $locale = null)
    {
        return $this->translator->transChoice($key, $count, $replace, $locale);
    }
}

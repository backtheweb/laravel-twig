<?php
namespace Backtheweb\Twig\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Twig_Environment
 * @see \Backtheweb\Twig\Rogin
 */
class Twig extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'twig';
    }
}
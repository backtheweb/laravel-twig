<?php

namespace Backtheweb\Twig\Tool;

class Importer
{
    public static function blade($pathName)
    {
        return new Importer\Blade($pathName);
    }
}
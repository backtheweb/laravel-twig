<?php

namespace Backtheweb\Twig\Tool\Importer;


class Blade
{
    protected $content;

    protected $pathName;

    public function __construct($pathName)
    {

        $this->pathName = $pathName;
        $this->content  = file_get_contents($this->pathName);

        $this->convert();
    }

    public function convert()
    {
        $this->_replace_extends();
        $this->_replace_yield();
        $this->_replace_section();
        $this->_replace_if();
        $this->_replace_guest();
        $this->_replace_misc();
        $this->_replace_print_vars_simple();
    }

    public function save()
    {
        $target = str_replace('blade.php', 'twig', $this->pathName);

        file_put_contents($target, $this->content);

        return $target;
    }

    protected function _replace_extends()
    {
        $content = $this->content;

        $matches = [];
        $pattern = '/(@extends)\(\'([a-zA-Z.]+)\'\)/';

        preg_match($pattern, $content, $matches);

        if (count($matches) === 3){

            $replace = "{% extends '{$matches[2]}' %}";
            $this->content = str_replace($matches[0], $replace, $content);
        }
    }

    protected function _replace_section()
    {
        $content = $this->content;
        $matches = [];
        $pattern = '/(@section)\(\'([a-zA-Z.]+)\'\)/';

        preg_match($pattern, $content, $matches);

        if (count($matches) === 3){

            $replace = "{% block {$matches[2]} %}";
            $content = str_replace($matches[0], $replace, $content);
        }

        $this->content = str_replace('@endsection', '{% endblock %}', $content);
    }


    protected function _replace_yield()
    {
        $content = $this->content;
        $matches = [];
        $pattern = '/(@yield)\(\'([a-zA-Z.]+)\'\)/';

        preg_match($pattern, $content, $matches);

        if (count($matches) === 3){

            $replace        = "{% block {$matches[2]} %}{% endblock %}";
            $this->content  = str_replace($matches[0], $replace, $content);
        }
    }

    protected function _replace_if()
    {
        $content = $this->content;
        $matches = [];
        $pattern = '/(@if)[\s]?\((.+)\)/';

        preg_match_all($pattern, $content, $matches);

        if (count($matches) === 3){


            $_search  = $matches[0];
            $_replace = $matches[2];

            for ($i = 0, $max = count($_search); $i<$max; $i++){

                $replace = "{% if {$_replace[$i]} %}";
                $content = str_replace($_search[$i], $replace, $content);
            }
        }

        $content = str_replace('@else',     '{% else %}',   $content);
        $content = str_replace('@endif', '{% endif %}',     $content);

        $this->content = $content;
    }

    protected function _replace_guest()
    {
        $content = $this->content;
        $content = str_replace('@guest',    '{% if auth_guest() %}',  $content);
        $content = str_replace('@else',     '{% else %}',             $content);
        $content = str_replace('@endguest', '{% endif %}',            $content);

        $this->content = $content;
    }

    protected function _replace_print_vars_simple()
    {
        $content = $this->content;
        $matches = [];
        $pattern = '/{{[\s?]((.+))[\s?]}}/';

        preg_match_all($pattern, $content, $matches);

        if (count($matches) === 3){

            $_search  = $matches[0];

            for ($i = 0, $max = count($_search); $i<$max; $i++){

                $replace = $_search[$i];
                $replace = str_replace('$',  '',  $replace);
                $replace = str_replace('->', '.', $replace);
                $content = str_replace($_search[$i], $replace, $content);
            }
        }

        $this->content = $content;
    }

    protected function _replace_misc()
    {
        $content = $this->content;
        $content = str_replace("{{ str_replace('_', '-', app()->getLocale()) }}", '{{ locale }}', $content);
        $content = str_replace("{{ Auth::user()->name }}", '{{ auth_user().name  }}',             $content);
        $content = str_replace('@csrf', '{{ csrf_field() }}',                                     $content);
        $content = str_replace('$errors->', 'errors.',                                            $content);
        $this->content = $content;
    }
}
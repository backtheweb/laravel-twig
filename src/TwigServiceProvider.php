<?php

namespace Backtheweb\Twig;


use Illuminate\View\ViewServiceProvider;
use InvalidArgumentException;

class TwigServiceProvider extends ViewServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadConfiguration();
        $this->registerExtension();
        $this->registerCommands();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerOptions();
        $this->registerLoaders();
        $this->registerEngine();
    }

    /**
     *
     */
    protected function loadConfiguration()
    {
        $configPath = __DIR__ . '/../config/twig.php';

        $this->publishes([$configPath => config_path('twig.php')], 'config');
    }

    /**
     * Register Twig in the Laravel View component.
     */
    protected function registerExtension()
    {
        $this->app['view']->addExtension( $this->app['twig.extension'], 'twig', function () {

            return $this->app['twig.engine'];
        });
    }

    /**
     * cli commands
     */
    protected function registerCommands()
    {
        /*
        $this->app->bindIf('command.twig.clean', function () {
            return new Command\Clear;
        });
         */

        if ($this->app->runningInConsole()) {
            $this->commands([
                Command\Clear::class,
            ]);
        }

    }

    /**
     * Register Twig config option bindings.
     *
     * @return void
     */
    protected function registerOptions()
    {
        $this->app->bindIf('twig.extension', function () {

            return $this->app['config']->get('twig.extension');
        });

        $this->app->bindIf('twig.options', function () {

            $options = $this->app['config']->get('twig.environment', []);

            if (!isset($options['cache']) || is_null($options['cache'])) {

                $options['cache'] = storage_path('framework/views/twig');
            }

            return $options;
        });

        $this->app->bindIf('twig.extensions', function () {

            $load    = $this->app['config']->get('twig.extensions', []);
            $options = $this->app['twig.options'];
            $isDebug = (bool) (isset($options['debug'])) ? $options['debug'] : false;

            if ($isDebug) {
                array_unshift($load, 'Twig_Extension_Debug');
            }

            return $load;
        });

        $this->app->bindIf('twig.lexer', function () {

            return null;
        });
    }
    /**
     * Register Twig loader bindings.
     *
     * @return void
     */
    protected function registerLoaders()
    {
        // The array used in the ArrayLoader
        $this->app->bindIf('twig.templates', function () {
            return [];
        });

        $this->app->bindIf('twig.loader.array', function ($app) {

            return new \Twig\Loader\ArrayLoader($app['twig.templates']);
        });

        $this->app->bindIf('twig.loader.viewfinder', function () {

            return new Twig\Loader(

                $this->app['files'],
                $this->app['view']->getFinder(),
                $this->app['twig.extension']
            );
        });

        $this->app->bindIf('twig.loader.svg', function() {

            $finder = $this->app['view']->getFinder();

            $finder->addLocation(public_path('img/svg'));
            $finder->addExtension('svg');

            return new Twig\Loader(

                $this->app['files'],
                $finder,
                'svg'
            );
        });

        $this->app->bindIf('twig.loader.fontawesome', function() {

            $finder = $this->app['view']->getFinder();

            $finder->addLocation(resource_path('fonts/fontawesome'));
            $finder->addExtension('svg');

            return new Twig\Loader(

                $this->app['files'],
                $finder,
                'svg'
            );
        });

        $this->app->bindIf('twig.loader', function () {

                return new \Twig\Loader\ChainLoader([
                    $this->app['twig.loader.array'],
                    $this->app['twig.loader.viewfinder'],
                ]);
            },
            true
        );
    }
    /**
     * Register Twig engine bindings.
     *
     * @return void
     */
    protected function registerEngine()
    {
        $this->app->bindIf('twig', function () {

                $extensions = $this->app['twig.extensions'];
                $lexer      = $this->app['twig.lexer'];

                $twig       = new LaravelTwig(
                    $this->app['twig.loader'],
                    $this->app['twig.options'],
                    $this->app
                );

                foreach ($extensions as $extension) {

                    if (is_string($extension)) {

                        try {

                            $extension = $this->app->make($extension);

                        } catch (\Exception $e) {

                            throw new InvalidArgumentException(

                                "Cannot instantiate Twig extension '$extension': " . $e->getMessage()
                            );
                        }

                    } elseif (is_callable($extension)) {

                        $extension = $extension($this->app, $twig);

                    } elseif (!is_a($extension, 'Twig_Extension')) {

                        throw new InvalidArgumentException('Incorrect extension type');
                    }

                    $twig->addExtension($extension);
                }

                if (is_a($lexer, 'Twig_LexerInterface')) {
                    $twig->setLexer($lexer);
                }

                return $twig;
            },
            true
        );

        $this->app->bindIf('twig.compiler', function () {

            return new Engine\Compiler($this->app['twig']);
        });

        $this->app->bindIf('twig.engine', function () {

            return new Engine\Twig(
                $this->app['twig.compiler'],
                $this->app['twig.loader.viewfinder'],
                $this->app['config']->get('twig.globals', [])
            );
        });
    }

    public function provides()
    {
        return [
            //'command.twig.clean',
            'twig',
            'twig.options',
            'twig.extension',
            'twig.extensions',
            'twig.lexer',
            'twig.templates',
            'twig.loader.array',
            'twig.loader.viewfinder',
            'twig.loader.fontawesome',
            'twig.loader.svg',
            'twig.loader',
            'twig.compiler',
            'twig.engine',
        ];
    }
}
